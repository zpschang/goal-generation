import os
import numpy as np
import cv2

font                   = cv2.FONT_HERSHEY_SIMPLEX
fontScale              = 1
fontColor              = (0,0,0)
lineType               = 2

class Visualizer():
    def __init__(self, export_path, width, height, framerate=20):
        self.obs = []
        self.export_path = export_path
        self.width, self.height = width, height
        self.framerate = framerate
        if not os.path.isdir(export_path):
            os.makedirs(export_path)

    def start(self, filename):
        path = os.path.join(self.export_path, filename)
        self.writer = cv2.VideoWriter(path, cv2.VideoWriter_fourcc(*'MP4V'), self.framerate, (self.width, self.height))

    def record(self, observation, info=None):
        color = np.zeros_like(observation)
        color[..., 0] = observation[..., 2]
        color[..., 1] = observation[..., 1]
        color[..., 2] = observation[..., 0]
        if not color.dtype is np.uint8:
            color = color.astype('uint8')
        if info:
            bottomLeftCornerOfText = (int(0.2 * self.width), int(0.85 * self.height))
            cv2.putText(color, info, bottomLeftCornerOfText, font, fontScale, fontColor, lineType)
        self.writer.write(color)

    def export(self):
        self.writer.release()